import { Service } from '../../common/service'
import { PlayBook } from './playbook.entity'
import { PlaybookRepository } from './playbook.repository'

export class PlaybookService extends Service<PlayBook> {
    repository: PlaybookRepository
    constructor (repository: PlaybookRepository) {
        super(repository)
        this.repository = repository
    }

    async getPlaybookByOrg () {
        const data = await this.repository.getPlaybookByOrganization()
        return data
    }
}
