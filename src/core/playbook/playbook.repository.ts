import { Repository } from '../../common/repository'
import { PlayBook } from './playbook.entity'

export interface PlaybookRepository extends Repository<PlayBook> {
    getPlaybookByOrganization: () => Promise<PlayBook[]>
}
