interface Role {
    roleId: string
    name: string
}

interface Action {
    index: number,
    name: string,
    description: string
}

interface StageRole {
    roleId: string,
    actions: Action[]
 }
 interface Outcome {
    name: string
    description: string
 }
interface Stage {
        name: string,
        roles: StageRole[],
        outcomes: Outcome[]
}
export interface PlayBook {
        id: string,
        name: string,
        ownerId: string,
        state: number,
        roles: Role[],
        stages: Stage[],
        createdAt: string,
        updatedAt: string,
        deletedAt: string
}
