import { startRestServer } from './config/server'

startRestServer().listen(5000, () => {
    console.log('server listen on port 5000')
})
