import { Request, Response, Router } from 'express'
import playbookRouter from './playbook/playbook.router'

const router = Router()

router.get('/', (req: Request, res: Response) => {
    res.json({ message: 'hola mundo' })
})

// Routes
// PlayBook Routes
router.use('/playbook', playbookRouter)

export default router

