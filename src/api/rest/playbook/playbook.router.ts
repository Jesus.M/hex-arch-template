import { Router } from 'express'
import playbookController from './playbook.controller'
const router = Router()

router.get('/', playbookController.find)
router.get('/:id', playbookController.findOne)
router.post('/', playbookController.create)
router.put('/', playbookController.update)
router.delete('/', playbookController.delete)

export default router

