
import { Controller } from '../../../common/controller'
import { PlayBook } from '../../../core/playbook/playbook.entity'
import { PlaybookService } from '../../../core/playbook/playbook.service'
import playbookRepository from '../../../data/playbook/firestore/playbook.datasource'

class PlayBookController extends Controller<PlayBook> {

}

const playbookService = new PlaybookService(playbookRepository)
export default new PlayBookController(playbookService)
