
import { config as dotenvConfig } from 'dotenv'
if (process.env.NODE_ENV !== 'production') {
    dotenvConfig()
}

export const config = {
    API: {
        name: process.env.API_NAME || '',
        url: process.env.API_URL || '',
        version: process.env.API_VERSION || ''
    },
    DB: {
        FIRESTORE: {
            host: process.env.FIRESTORE_HOST || '',
            user: process.env.FIRESTORE_USER || '',
            password: process.env.FIRESTORE_PASS || '',
            key: process.env.FIRESTORE_SECRET_KEY || ''
        },
        MONGO: {
            host: process.env.API_NAME || '',
            user: process.env.API_NAME || '',
            password: process.env.API_NAME || ''
        }
    }
}
