import express from 'express'
import cors from 'cors'
import router from '../../api/rest/index'
import { errorHandler } from '../../common/middlewares/errorHandler'

/**
 * Create and Start a REST Server with express
 * @returns {Express} Express app
 */
export const startRestServer = () => {
    const app = express()

    // middleware
    app.use(cors())
    app.use(express.json())
    // Router
    app.use('/api/v1', router)

    app.use(errorHandler)
    return app
}
