export class BaseError extends Error {
    code: number = 500
    constructor (message: string, code = 500) {
        super(message)
        this.code = code
    }
}
