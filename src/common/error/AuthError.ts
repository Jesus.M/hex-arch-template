import { BaseError } from './BaseError'

export class AuthError extends BaseError {
    constructor (message: string) {
        super(message, 401)
    }
}
