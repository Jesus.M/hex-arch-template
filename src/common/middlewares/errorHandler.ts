import { NextFunction, Request, Response } from 'express'
import { AuthError } from '../error/AuthError'
import { BaseError } from '../error/BaseError'
import { NotFoundError } from '../error/NotFound'

export const errorHandler = (err: BaseError, req: Request, res: Response, next: NextFunction) => {
    if (err instanceof NotFoundError || err instanceof AuthError) {
        res.status(err.code)
        res.json({ success: false, error: err.stack, message: err.message })
        return
    }
    next(err)
}
