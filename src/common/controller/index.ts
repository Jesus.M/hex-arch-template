import { Request, Response } from 'express'
import { NotFoundError } from '../error/NotFound'
import { Service } from '../service'
import 'express-async-errors'

export class Controller<T> {
    service: Service<T>
    constructor (service: Service<T>) {
        this.service = service
    }

    /**
     *  Find All Resources
     * @param {Request} req HTTP request
     * @param {Response} res HTTP response
     * @return {void} Express Response
     */
    find = async (req: Request, res: Response) => {
        const data = await this.service.findAll()
        if (!data.length) {
            throw new NotFoundError('playbooks not found')
        }
        res.json(data)
    }

    /**
     *  Find by Id
     * @param {Request} req HTTP request
     * @param {Response} res HTTP response
     * @return {void} Express Response
     */
    findOne = async (req: Request, res: Response) => {
        res.json(await this.service.findOne())
    }

    /**
     *  Save resource
     * @param {Request} req HTTP request
     * @param {Response} res HTTP response
     * @return {void} Express Response
     */
    create = async (req: Request, res: Response) => {
        res.json(await this.service.create())
    }

    /**
     *  Update a resource
     * @param {Request} req HTTP request
     * @param {Response} res HTTP response
     * @return {void} Express Response
     */
    update = async (req: Request, res: Response) => {
        res.json(await this.service.update())
    }

    /**
     *  Remove resource by id
     * @param {Request} req HTTP request
     * @param {Response} res HTTP response
     * @return {void} Express Response
     */
    delete = async (req: Request, res: Response) => {
        res.json(await this.service.delete())
    }
}
