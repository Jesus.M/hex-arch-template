export interface Repository<T> {
    findAll: () => Promise<T[]>
    findOne: () => Promise<T>
    save: () => Promise<T>
    update: () => Promise<T>
    delete: () => Promise<T>
}
