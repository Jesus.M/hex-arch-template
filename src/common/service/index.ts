import { Repository } from '../repository'

export class Service<T> {
    repository
    constructor (repository: Repository<T>) {
        this.repository = repository
    }

    /**
     *  Find All
     * @return {Promise<[]>} Data from repository
     */
    findAll = async () : Promise<T[]> => {
        return await this.repository.findAll()
    }

    /**
     *  Get resource by params
     * @return {Promise<[]>} Data from repository
     */
    findOne = async (): Promise<T> => {
        return await this.repository.findOne()
    }

    /**
     *  Save resource
     * @return {Promise<[]>} Data from repository
     */
    create = async (): Promise<T> => {
        return await this.repository.save()
    }

    /**
     *  update resource
     * @return {Promise<[]>} Data from repository
     */
    update = async (): Promise<T> => {
        return await this.repository.update()
    }

    /**
     *  delete resource by Id
     * @return {Promise<[]>} Data from repository
     */
    delete = async (): Promise<T> => {
        return await this.repository.delete()
    }
}
