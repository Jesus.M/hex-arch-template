import { PlayBook } from '../../../core/playbook/playbook.entity'
import { PlaybookRepository } from '../../../core/playbook/playbook.repository'

class PlaybookDataSource implements PlaybookRepository {
    async findAll () {
        return Promise.resolve([] as PlayBook[])
    }

    async findOne () {
        return Promise.resolve({} as PlayBook)
    }

    async save () {
        return Promise.resolve({} as PlayBook)
    }

    async update () {
        return Promise.resolve({} as PlayBook)
    }

    async delete () {
        return Promise.resolve({} as PlayBook)
    }

    async getPlaybookByOrganization () {
        return Promise.resolve([] as PlayBook[])
    }
}

export default new PlaybookDataSource()
